########################################################
# This script checks for cycle activity,if not running #
# it will copy code to area where mover.ksh can access #
# and utilize it for funct or release.                 #
# Note:An xml file is used to pass in relevant         #
# parameters for specific projects.                    #
#                                                      #
#                                                      #
#======================================================#
# Ver 1.0, initial write, 2/5/2015 - R. McBride        #
#                                                      #
# Ver 1.1, git diff to determine changes in commit ID  #
# and copy only files changed to release area.         #
# 2/11/2015 - R. McBride                               #
########################################################

#this has been edited for testing with POC jenkens machine


#hard coded variables
$time = (Get-Date).tostring("HH")
$cycle = 13, 14
$codelocation = (Get-Location).path
$projectfile = gci ($codelocation) -Name *.xml
$projectfilename = $projectfile.split(".")[0]

#passed in from jenkins env variables
$currentbuildcommit = $env:GIT_COMMIT # must run this script with %GIT_COMMIT% passed in at the end of the line
$branch = $env:GIT_BRANCH # must run this script with %GIT_BRANCH% passed in at the end of the line

#xml determined variables
[xml]$projectxml = Get-Content -Path $codelocation\$projectfilename.xml
$preworkspaceF = $projectxml.projxml.functxml.preworkspace
$postworkspaceF = $projectxml.projxml.functxml.postworkspace
$preworkspaceR = $projectxml.projxml.releasexml.preworkspace
$postworkspaceR = $projectxml.projxml.releasexml.postworkspace
$cifs = $projectxml.projxml.cifs
$project = $projectxml.projxml.project

#$prevbuildcommit = Get-Content '$preworkspace\lastbuild.txt'

#RUN THIS ONLY THE FIRST TIME write current build commit id to lastbuild.txt for next run
#        $lastbuildtext = New-Item -ItemType file "$preworkspace\lastbuild.txt"
#            Add-Content $lastbuildtext $currentbuildcommit
#RUN THIS ONLY THE FIRST TIME################################################################            
            


#check for running cycle
if ($time -eq $cycle)
{
    Write-Host #################################################
    write-host # Cycle is currently running. Cannot move code. #
    Write-Host #################################################
}
else
{
    Write-Host #############################################################
    write-host # Cycle is not running. Code will now move and post process #
    Write-Host #############################################################
    Write-Host 
        
        if ($branch -eq 'origin/funct')
        {
        
            #git diff commit id with previous build file put files in variable
            $diff = git diff --name-only (Get-Content $lastbuildtext) $currentbuildcommit
        
        
            #copy files in $diff variable into postworkspace
            Foreach ($filenames in $diff)
            {
                Copy-Item $filename $postworkspaceF
            }
        }
        elseif ($branch -eq 'origin/release')
        {
            #git diff commit id with previous build file put files in variable
            $diff = git diff --name-only (Get-Content $lastbuildtext) $currentbuildcommit
        
        
            #copy files in $diff variable into postworkspace
            Foreach ($filenames in $diff)
            {
                Copy-Item $filename $postworkspaceR
            }
        }
        else
        {
            Write-Host '================================================================'
            Write-host ' Neither Funct or Release branches were passed into variables   ' 
            Write-Host ' There is an issue with what was copied into workspace          '
            Write-Host ' Exiting now.                                                   '
            Write-Host '================================================================'
        }
        
        #write current build commit id to lastbuild.txt for next run
        $lastbuildtext = "$preworkspace\lastbuild.txt"
            Add-Content $lastbuildtext $currentbuildcommit
  
        #New-Item -ItemType file
        
        
        
}

